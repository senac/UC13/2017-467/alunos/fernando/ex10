/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.Alunos;
import br.com.senac.BoletimAlunos;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fernando
 */
public class AlunosTest {
    
    public AlunosTest() {
    }
    
    
    @Test
    public void deveVerificarQuantosAlunosTemNotaAcimaDe7(){
        Alunos alunos1 = new Alunos("Fernando", 8, 8);
        Alunos alunos2 = new Alunos("Felipy", 7, 8);
        Alunos alunos3 = new Alunos("Joao", 6, 5);
        
        List<Alunos> lista = new ArrayList<>();
        lista.add(alunos1);
        lista.add(alunos2);
        lista.add(alunos3);
        
        BoletimAlunos boletimAlunos = new BoletimAlunos();
        int resultado = boletimAlunos.calcularAlunos(lista);
        
        assertEquals(2, resultado, 0.01);
    }
}
