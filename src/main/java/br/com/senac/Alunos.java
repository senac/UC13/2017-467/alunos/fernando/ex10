/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

/**
 *
 * @author Fernando
 */
public class Alunos {

   private String aluno;
   private double nota;
   private double nota2;

    public Alunos() {
    }

    public Alunos(String aluno, double nota, double nota2) {
        this.aluno = aluno;
        this.nota = nota;
        this.nota2 = nota2;
    }

    public String getAluno() {
        return aluno;
    }

    public void setAluno(String aluno) {
        this.aluno = aluno;
    }

    public double getNota() {
        return nota;
    }

    public void setNota(double nota) {
        this.nota = nota;
    }

    public double getNota2() {
        return nota2;
    }

    public void setNota2(double nota2) {
        this.nota2 = nota2;
    }
    

    
}
